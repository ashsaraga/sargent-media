# Sargent Media

A media server from the Strange Bedfellows currently available at [https://sargent.mightyops.io](https://sargent.mightyops.io).

Sargent presently runs on a schedule of 0915 - 0045 EST.

---

# Moxie in Making 

Let the development mayhem begin. 

- **local IP:** `192.168.1.150`
- **global IP:** `71.135.20.254`
- **/etc/hosts:** `sargent`

**A quick overview:** 

- [Ubuntu Server](https://gitlab.com/ashsaraga/sargent-media#ubuntu-server-2004-focal-fossa) 
- [Samba Share](https://gitlab.com/ashsaraga/sargent-media#samba-share)
- [Plex Server](https://gitlab.com/ashsaraga/sargent-media#plex-media) 
- [Mount on Boot](https://gitlab.com/ashsaraga/sargent-media#mount-externals-on-boot) 
- [Auto Shutdown and Boot](https://gitlab.com/ashsaraga/sargent-media#automatic-wake-and-sleep) 

## Ubuntu Server [20.04 | Focal Fossa]

Running Ubuntu, especially considering Moxie's presently somewhat meager hardware, was the easy choice. 

#### method 
1. download disk image
2. burn to USB with BalenaEtcher
3. boot installer to Moxie
4. setup SSH access for Rosemary 

### notes 

- uploaded SSH keys for rosemary and minerva 
- created user group `media` for ease of permissions on the external drives 

### reference 

- [Install Ubuntu Server](https://ubuntu.com/tutorials/install-ubuntu-server) 
- [disk image](./ubuntu-20.04-live-server-amd64.iso)
- [create bootable usb](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#6-write-to-device) 
- [USB Mounting](https://linuxconfig.org/howto-mount-usb-drive-in-linux) 

## Samba Share 

We're gonna use Samba as an easy way to ensure local-level connectivity no matter what fuckery Ash gets up to with Plex.  Setup done through the Samba CLI. 

#### method 
###### 2020-11-25
1. `sudo apt update && sudo apt upgrade`: to always begin at the beginning 
2. `sudo apt install samba`: download and install the samba pkg 
3. `whereis samba`: quick check to make sure samba files were added, expect something like: `samba: /usr/sbin/samba` 
4. `sudo vi /etc/samba/smb.conf`: edit the default samba config 

##### `/etc/samba/smb.conf` addition 
```sh 
# USER CONFIG ADDITIONS
## @ash 2020-10-14
## @ash 2020-11-25
[sambashare]
  comment = samba@sargent
  path = /media/sargent
  read only = no
  browsable = yes
  valid users = sargent, ash
```

5. `sudo service smbd restart`: restart samba to apply new config 
6. `sudo ufw allow samba`: whitelist samba so it can talk with the network 
7. `sudo adduser sargent`: creating `sargent` user for non-Ash, non-sudo samba access 
8. `sudo smbpasswd -a sargent`: configure samba for `sargent` user 
9. `sudo smbpasswd -a ash`: configure samba for `ash` user 

### notes 

- the samba path is set to `/media/sargent` as that's where our external drives are automatically mounted 

### reference 

- [Samba via CLI Guide](https://help.ubuntu.com/community/How%20to%20Create%20a%20Network%20Share%20Via%20Samba%20Via%20CLI%20%28Command-line%20interface/Linux%20Terminal%29%20-%20Uncomplicated,%20Simple%20and%20Brief%20Way!)
- [`samba.conf` ref](https://www.samba.org/samba/docs/using_samba/ch06.html) 

## Plex Media 

Truth be told, my heart lies with Kodi for media management on Linux.  But after a long and arduous battle on the RasPi, I worried Moxie's limited computing power would bear much the same result.  That being said, I think there's a high likelihood for Sargent being moved to a Kodi instance once Moxie's hardward gets some love.  

#### method 

This was actually one of the easiest parts -- see [Install Plex on Ubuntu Server](https://www.linuxbabe.com/ubuntu/install-plex-media-server-ubuntu-20-04) for steps followed.  

### notes 

- Plex *could* use Moxie's bare IPv4, but I have a domain name and Apache soooo... yeah 

##### `/etc/apache2/sites-enabled/plex.conf` 
```sh 
<VirtualHost *:80>
   ServerName sargent.mightyops.io
   ErrorDocument 404 /404.html

   #HTTP proxy
   ProxyPass / http://localhost:32400/
   ProxyPassReverse / http://localhost:32400/

   #Websocket proxy
   SSLProxyEngine on
   <Location /:/websockets/notifications>
        ProxyPass wss://localhost:32400/:/websockets/notifications
        ProxyPassReverse wss://localhost:32400/:/websockets/notifications
   </Location>

   Header always unset X-Frame-Options
RewriteEngine on
RewriteCond %{SERVER_NAME} =sargent.mightyops.io
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
```

##### `/etc/apache2/sites-enabled/plex-le-ssl.conf` 
```sh 
<IfModule mod_ssl.c>
SSLStaplingCache shmcb:/var/run/apache2/stapling_cache(128000)
<VirtualHost *:443>
   ServerName sargent.mightyops.io
   ErrorDocument 404 /404.html

   #HTTP proxy
   ProxyPass / http://localhost:32400/
   ProxyPassReverse / http://localhost:32400/

   #Websocket proxy
   SSLProxyEngine on
   <Location /:/websockets/notifications>
        ProxyPass wss://localhost:32400/:/websockets/notifications
        ProxyPassReverse wss://localhost:32400/:/websockets/notifications
   </Location>

   Header always unset X-Frame-Options


Include /etc/letsencrypt/options-ssl-apache.conf
Header always set Strict-Transport-Security "max-age=31536000"
SSLUseStapling on
SSLCertificateFile /etc/letsencrypt/live/sargent.mightyops.io/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/sargent.mightyops.io/privkey.pem
</VirtualHost>
</IfModule>
```

### reference 
- [Ubuntu Tut](https://ubuntu.com/tutorials/install-and-configure-samba#2-installing-samba)
- [Samba Server Guide](https://help.ubuntu.com/community/Samba/SambaServerGuide?_ga=2.48382566.1761503373.1606324883-236828187.1596378494)

## `mount` Externals on Boot 

Since all of our auxhiliary storage exists as external drives, we need to make sure Ubuntu is mounting them any time Moxie boots up. Best way to do this is by utilizing the `fstab`.  

Regarding the `fstab` settings for individual drives: thus far, I've arrived at these settings after much trial-and-error and they exist in the realm of code we'll call "Clark's Third."  Filesystem was by far the most important starting point, which you can use to verify the disk and begin constructing your `fstab` addition.  From there, check against the code you've already done for other drives, and flip through the resource links for anything you might be missing. 

#### method 
###### 2021-02-16 
1. `sudo mkdir /media/sargent sudo mkdir`: creating a top-level folder inside Moxie's default media directory, just to keep things nice and origanized 
2. `sudo mkdir /media/sargent/sg-tv`, `sudo mkdir /media/sargent/sg-movies`, `sudo mkdir /media/sargent/sg-tv.1`: create a mount point for each external drive  
3. `ll /media/sargent`: verify mount points and permissions -- best results have been owner `sargent:media` and permissions `774`, though depending on each drive's filesystem, the ftsab will set these permissions for us on mount 
4. `sudo fdisk -l`: get a list of available devices and ID the externals 

##### `sudo fdisk -l` 
```sh 
# sg-tv.0 [sda1] 
Disk /dev/sda: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: Portable SSD T5
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0xc5037488

Device     Boot Start        End    Sectors   Size Id Type
/dev/sda1        2048 1953522112 1953520065 931.5G  7 HPFS/NTFS/exFAT

# sg-tv.1 [sdc1] 
Disk /dev/sdc: 1.37 TiB, 1500301910016 bytes, 2930277168 sectors
Disk model: WDC WD15EARS-00Z
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: A8246CD5-A26A-4848-9845-8B4170F10627

Device     Start        End    Sectors  Size Type
/dev/sdc1   2048 2930276351 2930274304  1.4T Linux filesystem

# sg-movies.0 [sdd3] 
Disk /dev/sdd: 931.49 GiB, 1000170586112 bytes, 1953458176 sectors
Disk model: My Passport 0824
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: C19FBE8D-0DD3-4AAB-9F3F-41B4556BA217

Device         Start        End    Sectors   Size Type
/dev/sdd1         40     409639     409600   200M EFI System
/dev/sdd2     409640  684353415  683943776 326.1G unknown
/dev/sdd3  684615680 1953193983 1268578304 604.9G Microsoft basic data
```

5. `sudo blkid`: the absolute best way to make sure we're always selecting the right drive and partition is to call them by UUID, which `blkid` will give us. 

```sh
# sg-tv.0 
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"

# sg-tv.1 
/dev/sdc1: LABEL="datapartition" UUID="d2fce0a4-e542-44fd-bd4e-e0828091cafa" TYPE="ext4" PARTLABEL="primary" PARTUUID="e6621ad7-dd16-4627-b9fa-69ad04b8b3db"

# sg-movies.0 
/dev/sdd3: LABEL_FATBOOT="RASPI" LABEL="SG-MOVIES" UUID="05EC-1B12" TYPE="vfat" PARTUUID="cac5542e-c865-4d37-a179-8bc92a68edf5"
```

6. `sudo vi /etc/fstab`: update the `fstab` with details for each external drive 

```sh 
# auto-mounting sg-tv.1
UUID=d2fce0a4-e542-44fd-bd4e-e0828091cafa /media/sargent/sg-tv.1 ext4 defaults 0 0

# auto-mounting sg-movies.0
UUID=05EC-1B12 /media/sargent/sg-movies vfat rw,auto,user,users,gid=1002,umask=002 0 0

# auto-mounting sg-tv.0
UUID=A45CC03A5CC008CE /media/sargent/sg-tv ntfs rw,auto,user,users,uid=1001,gid=1002,umask=003 0 0
```

7. `sudo mount -a`: check for errors in the `/etc/fstab` 

### reference 

- [fstab docs](https://wiki.debian.org/fstab) 
- [`mount` docs](https://help.ubuntu.com/community/Mount/USB) 

## Automatic Wake and Sleep


###### 2021-02-17 

We've finally done the thing and now the Plex server is all set to go.  Which, honestly, is pretty exciting.  I did some good (if not incredibly slow) work.

Now it's just down to automatically sleeping and waking Moxie, which I'm going to accomplish with `rtcwake` and a cron job.

The following steps were taken in accordance with [this](https://askubuntu.com/questions/567955/automatic-shutdown-at-specified-times) forum post:

> #### Cron
> 
> You can create a root cron job that calls this script to execute at a specific time in the evening and then awake in the morning:
> 
> `sudo crontab -e`
> 
> Now enter something like to run the suspend script at 23:30:
> 
> `30 23 * * * /home/myhomefolder/suspend_until 07:30` 

> #### suspend_until script
> 

```sh
#!/bin/bash 

# Auto suspend and wake-up script
#
# Puts the computer on standby and automatically wakes it up at specified time
#
# Written by Romke van der Meulen <redge.online@gmail.com>
# Minor mods fossfreedom for AskUbuntu
#
# Takes a 24hour time HH:MM as its argument
# Example:
# suspend_until 9:30
# suspend_until 18:45

# ------------------------------------------------------
# Argument check
if [ $# -lt 1 ]; then
    echo "Usage: suspend_until HH:MM"
    exit
fi

# Check whether specified time today or tomorrow
DESIRED=$((`date +%s -d "$1"`))
NOW=$((`date +%s`))
if [ $DESIRED -lt $NOW ]; then
    DESIRED=$((`date +%s -d "$1"` + 24*60*60))
fi

# Kill rtcwake if already running
sudo killall rtcwake

# Set RTC wakeup time
# N.B. change "mem" for the suspend option
# find this by "man rtcwake"
sudo rtcwake -l -m off -t $DESIRED &

# feedback
echo "Suspending..."

# give rtcwake some time to make its stuff
sleep 2

# then suspend
# N.B. dont usually require this bit
#sudo pm-suspend

# Any commands you want to launch after wakeup can be placed here
# Remember: sudo may have expired by now

# Wake up with monitor enabled N.B. change "on" for "off" if 
# you want the monitor to be disabled on wake
# xset dpms force on

# and a fresh console
clear
echo "Good morning!"
```

> N.B.
> 
> Change mem in this part of the script for whatever suspend method works for you:

```sh
# Set RTC wakeup time
sudo rtcwake -l -m mem -t $DESIRED &
```

> You may also have to substitute the `-u` flag in place of the `-l` flag depending on whether your hardware clock uses UTC (`-u`) or local (`-l`) time. Note that your hardware clock is different from the system clock you see in your operating system.

##### crontab addition

```sh
# 2021-02-18
# automatic poweroff and boot
45 00 * * * /home/ash/bin/suspend_until 09:15
```

### notes 


### reference 

- [automatic shutdown and boot](https://askubuntu.com/questions/567955/automatic-shutdown-at-specified-times) 

--- 

# Maintenance Notes 

## Giving Jeff Upload Access 
###### 2021-03-27 
1. `sudo adduser jeff`: create `jeff` user for samba access
2. `sudo su - jeff`: switch to user `jeff` 
3. `ssh-keygen`: create ssh infrastructure 
4. `touch ~/.ssh/authorized_keys`: create key file for Jeff's `id_rsa.pub` 
5. `exit`: switch back to `ash`  
6. `sudo usermod -aG media jeff`: add `jeff` to `media` group 

---

# Reference 

## Online Resources 

- [Ubuntu: Mount USB](https://help.ubuntu.com/community/Mount/USB)
- [LinuxBabe: Plex on Ubuntu 20.04](https://www.linuxbabe.com/ubuntu/install-plex-media-server-ubuntu-20-04)
- [itsFoss: Share on Local Newtork, Ubuntu to Windows](https://itsfoss.com/share-folders-local-network-ubuntu-windows/)
- [LinuxConfig: Configure Samba on Ubuntu 20.04](https://linuxconfig.org/how-to-configure-samba-server-share-on-ubuntu-20-04-focal-fossa-linux)
- [WebforStudents: Install Samba on Ubuntu 20.04](https://websiteforstudents.com/install-samba-on-ubuntu-20-04-18-04/)
- [Ubuntu: Install and Config Samba](https://ubuntu.com/tutorials/install-and-configure-samba#1-overview)
- [List Linux Users](https://linuxize.com/post/how-to-list-users-in-linux/)
- [Add User to Group](https://phoenixnap.com/kb/how-to-add-user-to-group-linux#htoc-add-an-existing-user-to-an-existing-group)
- [Add User to Sudoers](https://phoenixnap.com/kb/how-to-create-sudo-user-on-ubuntu)
- [Permission Calculator](http://permissions-calculator.org/) 
- [Linux File Permissions](https://www.pluralsight.com/blog/it-ops/linux-file-permissions)
- [Chmod Mounted Drives](https://stackoverflow.com/questions/20584032/chmod-a-freshly-mounted-external-drive-to-set-up-writing-access) 

## Troubleshooting History

Prepare for some small measure of useful info, but a lot of nonsense and creative profanity. 

---

#### mount  
###### 2020-11-13 
1. `sudo apt-get upgrade` 
2. `sudo apt-get update` 
3. `sudo mount /dev/sdd3 /media/sg-movies/ -o uid=1000,gid=1000,utf8,dmask=027,fmask=137` 
4. `sudo mount /dev/sda1 /media/sg-tv/ -o uid=1000,gid=1000,utf8,dmask=027,fmask=137` 
5. `mkdir /media/sg-tv/tv/newsradio.1995`
6. transferred some NewsRadio from rosemary 

---

#### mount / fstab 
###### 2020-11-23 
1. booted up Mox and fan began making a loud and worrisome noise 
2. opened her up, whapped on the fan a few times, all mechanical parts now seem to be in order 
3. booted Mox back up but can't seem to get a response when attempting to SSH in: `ssh: connect to host 192.168.1.150 port 22: Host is down` 
4. as per [some responses](https://www.reddit.com/r/raspberry_pi/comments/3m3sdm/host_is_down_when_trying_to_ssh_in_except_it_isnt/) on the RasPi subreddit, it seems like the IP address might have gotten changed since it's getting its address from the dumb AT&T router 
5. OH RIGHT -- I disconnected Mox from the ethernet cord so I could move her into the kitchen for some hardware work -- let's start by reconnecting to the network, yah dip 
  * remember though, we are eventually going to need a static IP address for remote access
6. And just like that, we're connected
7. I think it's time to reassemble the case and reconnect the hard drives: `sudo poweroff` 
8. next steps today are: 
  - [x] check hard drives for storage space 
  - [ ] move media from Rosemary to Moxie 
  - [ ] add script for just after boot to automatically mount hard drives 
  - [ ] assign static IP address 
  - [ ] research some low-power running options (or run on a schedule options)
9. `sudo fdisk -l`: tv still registers as `sda1` but movies are now `sdc3` 
  * this means that the hard drives aren't always labeled the same by Ubuntu, but the ports they connect to might be 
10. `sudo mount /dev/sdc3 /media/sg-movies/`: mount movies 
11. `sudo mount /dev/sda1 /media/sg-tv/`: mount tv 
12. `df -h`: get disk usage stats
  - **95G** left on `sg-tv`
  - **449G** left on `sg-movies`
13. *ash@rosemary:*:`du -sh ~/sargent/tv`: 228G in TV to transfer -- going to have to set up the third hard drive 
14. *ash@rosemary:*:`du -sh ~/sargent/movies`: meanwhile, 23G in movies and we've got plenty of room to go 
15. breaking out the large grey harddrive and connecting to Rosemary for verifying -- okay, so turns out the big hard drive needs its own powersource and has only 250G of capacity... yeah, we're gonna pass 
16. looking into swapping the two hard drives we currently have, and/or installing Minerva's extra HDD in Moxie
17. SIGH, okay, hard drive swap won't work, and I'm pretty sure I have Ubuntu installed on Minerva's extra HDD -- time to break the rules 
18. `mkdir /media/sg-movies/tv_overflow`: just *FOR NOW* we'll be storing the tv shows currently on Rosemary in an overflow directory on the movies drive
19. transferring movies first:
  - `scp -r Aliens\ DC\ \(1986\)\ \[1080p\] ash@sargent:/media/sg-movies/movies`: GODSDAMNED PERMISSIONS ERROR JFC ALREADY
*20. `sudo umount /dev/sdc3`, `sudo chmod -R 766 sg-movies`, `sudo chown -R ash:media sg-movies`, `sudo mount /dev/sdd3 /media/sg-movies/`*
20. The above permissions mess didn't work because upon mounting the drive (which must be done as root), the permissions are essentially overwritten — I did, however, find out about the `/etc/fstab` file #auto_mounting !!!: `sudo umount /dev/sdd3`
21. `sudo blkid`: list disk UUIDs; 
  - `/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"`
  - `/dev/sdd3: LABEL_FATBOOT="RASPI" LABEL="SG-MOVIES" UUID="05EC-1B12" TYPE="vfat" PARTUUID="cac5542e-c865-4d37-a179-8bc92a68edf5"`
22. `sudo vi /etc/fstab`:
  - `UUID=05EC-1B12 /media/sg-movies vfat rw,auto,users,uid=1000,gid=1002,umask=003 0 0`
  - `UUID=A45CC03A5CC008CE /media/sg-tv ntfs rw,auto,users,uid=1000,gid=1002,umask=003 0 0`
23. `sudo reboot`: alright, it's go time 
24. **FUCK YES SUCCESS** 
25. let's get copying: 
  - `scp -r Aliens\ DC\ \(1986\)\ \[1080p\] ash@sargent:/media/sg-movies/movies`
  - `scp -r * ash@sargent:/media/sg-movies/movies` 

--- 

#### mount / fstab 
###### 2021-02-16 

1. `sudo apt-get update`; `sudo apt-get upgrade` 
2. Looks like SG-TV was automatically mounted but SG-MOVIES was not.
3. `sudo blkid`: 

```sh
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"
/dev/sr0: UUID="2009-11-18-15-47-00-00" LABEL="TEW-649UB" TYPE="iso9660"
/dev/sdb1: UUID="3ECB-6C70" TYPE="vfat" PARTUUID="e6b66ab8-380d-4b33-b8c1-b4eead9d5ecf"
/dev/sdb2: UUID="3084dc06-940e-4782-91b0-cec127eb31ea" TYPE="ext4" PARTUUID="28e99368-e8b8-4785-adf7-0cdf1891ed1c"
/dev/sdb3: UUID="8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz" TYPE="LVM2_member" PARTUUID="d27c5ab0-7b70-46d2-8caa-e3ed4d01e46d"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/loop3: TYPE="squashfs"
/dev/loop4: TYPE="squashfs"
/dev/loop6: TYPE="squashfs"
/dev/loop7: TYPE="squashfs"
```

4. Alright, it looks like SG-MOVIES is registering with a UUID of `3ECB-6C70` but is being called in `/etc/fstab` with `05EC-1B12`.
5. `sudo vi /etc/fstab`: updating SG-MOVIES UUID to `3ECB-6C70` 
6. `sudo reboot`: let's see if it worked 
7. Soooooo, it worked as I directed, but I directed incorrectly.  The UUID I provided was for an Ubuntu boot partition, heh.  Let's try that again.
8. `sudo fdisk -l`: so I think now that we need `sdb3`, which has a UUID of `8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz`.
9. `sudo vi /etc/fstab`: updating SG-MOVIES UUID to `8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz` 
10. `sudo reboot`: once more with feeling 
11. Alright, that didn't work.  The main issue right now is that I'm only 75% certain that I'm aiming for the right bit of the right hard drive, which is non-ideal.  So I'm going to try mounting it directly.
12. `sudo mount /dev/sdb3 /media/usb1/`: returns with error `mount: /media/usb1: unknown filesystem type 'LVM2_member'`.  
13. Troubleshooting: first lead seems promising, as it concerns assigning the same name to a different UUID after making a system update [link](https://askubuntu.com/questions/766048/mount-unknown-filesystem-type-lvm2-member) 

> You used the exact same name (ubuntu-vg) for your new volume group as the old volume group. You must give them unique names. You can rename one of the groups using vgrename and its UUID.
> 
> Find the UUID with vgdisplay and then rename the volume group:
> 
> `vgrename <VG UUID> new_name`

- so based on some of the other answers I found, we're proceeding with:

14. `sudo lvdisplay`: 

```
/dev/sdc: open failed: No medium found
/dev/sdc: open failed: No medium found
--- Logical volume ---
LV Path                /dev/ubuntu-vg/ubuntu-lv
LV Name                ubuntu-lv
VG Name                ubuntu-vg
LV UUID                I1RRVy-htyj-G55k-oIUI-TSGV-XBHQ-ORxS4G
LV Write Access        read/write
LV Creation host, time ubuntu-server, 2020-08-04 20:24:20 +0000
LV Status              available
# open                 1
LV Size                200.00 GiB
Current LE             51200
Segments               1
Allocation             inherit
Read ahead sectors     auto
- currently set to     256
Block device           253:0
```


15. *I JUST REALIZED SOMETHING*: originally the drive I'm looking for was mounting as `sdc`, which is no longer listed.  I assumed that the drive had simply been reassigned (seeing as I still don't entirely understand how the drive assignments work), but now I think that's wrong.  I think now it's showing up as `/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"`, which has the same UUID as the drive in question and better aligns with the responses I found in this first post.
16. `sudo vi /etc/fstab`: first I'm going to undo the changes made to `/etc/fstab`, so that the UUID is back to `e8631758-5aed-4876-9c62-9b08089cc5fd`.
17. `sudo vgrename e8631758-5aed-4876-9c62-9b08089cc5fd sg_movies`: now we rename the overlapping volume group name, which returns an error.
18. `sudo reboot`: trying first with reset `/etc/fstab` 
19. `sudo blkid` 

```
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"
/dev/sr0: UUID="2009-11-18-15-47-00-00" LABEL="TEW-649UB" TYPE="iso9660"
/dev/sdb1: UUID="3ECB-6C70" TYPE="vfat" PARTUUID="e6b66ab8-380d-4b33-b8c1-b4eead9d5ecf"
/dev/sdb2: UUID="3084dc06-940e-4782-91b0-cec127eb31ea" TYPE="ext4" PARTUUID="28e99368-e8b8-4785-adf7-0cdf1891ed1c"
/dev/sdb3: UUID="8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz" TYPE="LVM2_member" PARTUUID="d27c5ab0-7b70-46d2-8caa-e3ed4d01e46d"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/loop3: TYPE="squashfs"
/dev/loop4: TYPE="squashfs"
/dev/loop5: TYPE="squashfs"
```

20. `sudo vi /etc/fstab`: Issue is persisting, so now I'm going to omit the UUID from `/etc/fstab` and see what comes of it.
21. `sudo reboot`: goooos fraba 
22. `sudo blkid`: returns the same as before.
23. `sudo apt list --upgradable`: looks like we have several items that need explicit upgrade, so let's try that.
24. `sudo apt upgrade`: somehow a ton of upgrades to do despite being fairly certain I did this already. 
25. `sudo reboot`: for shits and gigs and maybe sanity 
26. `sudo poweroff`: alright, I'm physically removing the problem drive to get some harder facts 
27. Checking `SG-MOVIES` on Rosemary reveals a few things:
  - File system : MS-DOS (FAT32) 
  - File system UUID : 5C594FBE-3AAF-38EB-816D-97AA9DFEBFBC
28. `sudo fdisk -l`: let's see what we get with the hard drive disconnected. 

```
Disk /dev/sda: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: Portable SSD T5
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0xc5037488

Device     Boot Start        End    Sectors   Size Id Type
/dev/sda1        2048 1953522112 1953520065 931.5G  7 HPFS/NTFS/exFAT


Disk /dev/sdb: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: WDC WD5000AAKX-3
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: D1401A76-8F9E-4A02-8889-392FEDF459C8

Device       Start       End   Sectors   Size Type
/dev/sdb1     2048   1050623   1048576   512M EFI System
/dev/sdb2  1050624   3147775   2097152     1G Linux filesystem
/dev/sdb3  3147776 976771071 973623296 464.3G Linux filesystem


Disk /dev/mapper/ubuntu--vg-ubuntu--lv: 200 GiB, 214748364800 bytes, 419430400 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes 
```

Alright, so we have confirmation that `SG-MOVIES` is neither `sdb` or `/dev/mapper`.  

29. `sudo blkid`: 

```
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"
/dev/sr0: UUID="2009-11-18-15-47-00-00" LABEL="TEW-649UB" TYPE="iso9660"
/dev/sdb1: UUID="3ECB-6C70" TYPE="vfat" PARTUUID="e6b66ab8-380d-4b33-b8c1-b4eead9d5ecf"
/dev/sdb2: UUID="3084dc06-940e-4782-91b0-cec127eb31ea" TYPE="ext4" PARTUUID="28e99368-e8b8-4785-adf7-0cdf1891ed1c"
/dev/sdb3: UUID="8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz" TYPE="LVM2_member" PARTUUID="d27c5ab0-7b70-46d2-8caa-e3ed4d01e46d"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/loop3: TYPE="squashfs"
/dev/loop4: TYPE="squashfs"
/dev/loop5: TYPE="squashfs"
```

Alright, so a UUID I tried using is reliably being assigned to the Ubuntu drive, and now it should be assumed that was the wrong thing to use.

30. `sudo poweroff`: shutting down to reattach the `SG-MOVIES` drive now that it's been verified on Rosemary. 
31. `sudo fdisk -l`: 

```
Disk /dev/sda: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: Portable SSD T5
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0xc5037488

Device     Boot Start        End    Sectors   Size Id Type
/dev/sda1        2048 1953522112 1953520065 931.5G  7 HPFS/NTFS/exFAT


Disk /dev/sdb: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: WDC WD5000AAKX-3
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: D1401A76-8F9E-4A02-8889-392FEDF459C8

Device       Start       End   Sectors   Size Type
/dev/sdb1     2048   1050623   1048576   512M EFI System
/dev/sdb2  1050624   3147775   2097152     1G Linux filesystem
/dev/sdb3  3147776 976771071 973623296 464.3G Linux filesystem


Disk /dev/mapper/ubuntu--vg-ubuntu--lv: 200 GiB, 214748364800 bytes, 419430400 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sdd: 931.49 GiB, 1000170586112 bytes, 1953458176 sectors
Disk model: My Passport 0824
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: C19FBE8D-0DD3-4AAB-9F3F-41B4556BA217

Device         Start        End    Sectors   Size Type
/dev/sdd1         40     409639     409600   200M EFI System
/dev/sdd2     409640  684353415  683943776 326.1G unknown
/dev/sdd3  684615680 1953193983 1268578304 604.9G Microsoft basic data
```

32. `sudo blkid`: 

```
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"
/dev/sr0: UUID="2009-11-18-15-47-00-00" LABEL="TEW-649UB" TYPE="iso9660"
/dev/sdb1: UUID="3ECB-6C70" TYPE="vfat" PARTUUID="e6b66ab8-380d-4b33-b8c1-b4eead9d5ecf"
/dev/sdb2: UUID="3084dc06-940e-4782-91b0-cec127eb31ea" TYPE="ext4" PARTUUID="28e99368-e8b8-4785-adf7-0cdf1891ed1c"
/dev/sdb3: UUID="8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz" TYPE="LVM2_member" PARTUUID="d27c5ab0-7b70-46d2-8caa-e3ed4d01e46d"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"
/dev/sdd1: LABEL_FATBOOT="EFI" LABEL="EFI" UUID="67E3-17ED" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="054feb27-a953-41b0-b002-0cc49dec1066"
/dev/sdd2: UUID="e51a3730-3a92-4ad3-a123-e6db66c22b98" TYPE="apfs" PARTLABEL="TARDIS" PARTUUID="8b7a43dc-8748-4fb4-a880-ff7fb4bcfe88"
/dev/sdd3: LABEL_FATBOOT="RASPI" LABEL="SG-MOVIES" UUID="05EC-1B12" TYPE="vfat" PARTUUID="cac5542e-c865-4d37-a179-8bc92a68edf5"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/loop3: TYPE="squashfs"
/dev/loop4: TYPE="squashfs"
/dev/loop5: TYPE="squashfs"
```

Looks like the UUID we want is `05EC-1B12`, which was the original UUID from sessions ago.  My best guess now is that the hard drive got partly disconnected and wasn't being loaded onto the system.  That being said: 

33. `sudo vi /etc/fstab`: uncommenting `SG-MOVIES` line and changing UUID to `05EC-1B12`. 
34. `sudo reboot`: with intrepid hope 
35. `ls media/sg-movies`: **SUCCESS** 

**Moxie is now automatically mounting `SG-MOVIES` and `SG-TV` automatically with `774` permissions for `ash:media`.** 

---

#### samba 

###### 2021-02-16 
1. `whereis samba`: results in `samba: /usr/sbin/samba /usr/lib/x86_64-linux-gnu/samba /etc/samba /usr/share/samba /usr/share/man/man7/samba.7.gz /usr/share/man/man8/samba.8.gz` 
2. `sudo vi /etc/samba/smb.conf`: 

```
# USER CONFIG ADDITIONS
## @ash 2020-10-14
## @ash 2020-11-25
[sambashare]
  comment = samba@sargent
  path = /home/sargent/media
  read only = no
  browsable = yes
  valid users = sargent, ash
```

This basic framework seems to have come from [here](https://ubuntu.com/tutorials/install-and-configure-samba#2-installing-samba), but after reading the docs [here](https://help.ubuntu.com/community/Samba/SambaServerGuide?_ga=2.48382566.1761503373.1606324883-236828187.1596378494) I think that was an inadequate setup.

3. `sudo smbpasswd -e sargent;sudo smbpasswd -e ash`: Enabled user sargent;Enabled user ash.
4. `sudo su - sargent`: switching to `sargent` user to confirm media symlinks and access to mounted drives
5. `ln -s /media/sg-tv/tv sg-tv`: creating `TV` directory 
6. `exit; sudo service smbd restart`: switching back to ash and restarting samba 
7. `sudo smbd reload`: reloading because that's what the guide actually says to do 
8. Can now connect to the share with `ash` and `sargent` users, but no files are listed.  I think the problem is stemming from the symlinks.
9. `cp /media/sg-movies/movies/Baby-Driver.2017.mkv .`: testing reveals that it is, indeed, the symlinks.  Baby Driver plays on Rosemary with no issues.
10. `sudo vi /etc/fstab`: updating mounting paths to the samba share directory.  This may require the `[home]` settings to be set explicitly in samba's config.  We'll see.
11. `sudo reboot`: nothing like breaking it when it only just began working 
12. `sudo su - sargent; ls media`: yeah alright, like literally nothing works.  
13. `exit; sudo vi /etc/fstab;sudo reboot`: Resetting and restarting. 
14. `ls media`: alright everything seems back to normal.
15. `sudo vi /etc/fstab; sudo mkdir sargent; sudo chown ash:media sargent/; sudo chmod 774 sargent/`: created a directory in `/media` for the drives to mount in, so that samba doesn't have access to the entire top-level folder. 
16. `sudo reboot`: because why leave well enough alone 
17. `ls /media/sargent`: alright, mounting works, now it's down to samba 
18. `sudo vi /etc/samba/smb.conf`: updating samba's share path 
19. `sudo service smbd restart`: restarting to enact changes 
20. Alright, **partial success**!  Checking it out on Rosemary, `sargent` user can view the TV Shows without issue, but doesn't have permissions for the Movies. That is, until I inspect the info panel, which is triggering the correct permissions for... reasons?
21. It looks like `ash` user has access straight off, and I'm betting there's some cross authorization happening with the inspection panel.  So! I want to test if this is an ownership issue.
22. `sudo vi /etc/fstab`: changing uid to `1001` so sargent will own the mounted drives 
23. `sudo reboot`: checking mounted drives, everything looks as it should 
24. Connecting on Rosemary creates the same issue, and I'm beginning to think it's a Mac OS issue.  Not quite sure on the deets beyond that, but I'm gonna try connect on a different device and see what happens 
25. Yeah alright, I connect as `sargent` on Rowena and had full access no problem.  Which also means (for the most part) **SUCCESS**.

##### unaccounted for snippet 
```sh
sudo setfacl -m u:plex:rx /media/sargent/sg-tv
sudo setfacl -m u:plex:rx /media/sargent/sg-movies

setfacl: Operation not supported
```

---

#### mount / fstab 

###### 2021-03-17 

## Installing Internal WD 1.5T 

Right, I tried this sort of "off the cuff" and immediately regretted it due to:

- Moxie boots CRAZY slow 
- sg-movies.0 became inaccessible 
- WD drive wouldn't mount as writable 
- various other annoying things 

That means, now that I've fixed my fuckery, it's time to try this again but with notes.  And research.  Like I should have the first time.

### Before Drive Istallation 

#### `sudo fdisk -l` 

```
Disk /dev/loop0: 55.39 MiB, 58073088 bytes, 113424 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop1: 55.48 MiB, 58159104 bytes, 113592 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop2: 72.4 MiB, 75534336 bytes, 147528 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop3: 32.28 MiB, 33845248 bytes, 66104 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop4: 31.9 MiB, 32595968 bytes, 63664 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop5: 72.5 MiB, 75546624 bytes, 147552 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sda: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: Portable SSD T5
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0xc5037488

Device     Boot Start        End    Sectors   Size Id Type
/dev/sda1        2048 1953522112 1953520065 931.5G  7 HPFS/NTFS/exFAT


Disk /dev/sdb: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: WDC WD5000AAKX-3
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: D1401A76-8F9E-4A02-8889-392FEDF459C8

Device       Start       End   Sectors   Size Type
/dev/sdb1     2048   1050623   1048576   512M EFI System
/dev/sdb2  1050624   3147775   2097152     1G Linux filesystem
/dev/sdb3  3147776 976771071 973623296 464.3G Linux filesystem


Disk /dev/mapper/ubuntu--vg-ubuntu--lv: 200 GiB, 214748364800 bytes, 419430400 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sdc: 931.49 GiB, 1000170586112 bytes, 1953458176 sectors
Disk model: My Passport 0824
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: C19FBE8D-0DD3-4AAB-9F3F-41B4556BA217

Device         Start        End    Sectors   Size Type
/dev/sdc1         40     409639     409600   200M EFI System
/dev/sdc2     409640  684353415  683943776 326.1G unknown
/dev/sdc3  684615680 1953193983 1268578304 604.9G Microsoft basic data
```

#### `sudo blkid` 

```
/dev/sda1: LABEL="SG-TV" UUID="A45CC03A5CC008CE" TYPE="ntfs" PARTUUID="c5037488-01"
/dev/sdb1: UUID="3ECB-6C70" TYPE="vfat" PARTUUID="e6b66ab8-380d-4b33-b8c1-b4eead9d5ecf"
/dev/sdb2: UUID="3084dc06-940e-4782-91b0-cec127eb31ea" TYPE="ext4" PARTUUID="28e99368-e8b8-4785-adf7-0cdf1891ed1c"
/dev/sdb3: UUID="8Gsnel-uTds-Hbti-K7D9-9Bn8-7jWQ-UbDPLz" TYPE="LVM2_member" PARTUUID="d27c5ab0-7b70-46d2-8caa-e3ed4d01e46d"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="e8631758-5aed-4876-9c62-9b08089cc5fd" TYPE="ext4"
/dev/sdc1: LABEL_FATBOOT="EFI" LABEL="EFI" UUID="67E3-17ED" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="054feb27-a953-41b0-b002-0cc49dec1066"
/dev/sdc2: UUID="e51a3730-3a92-4ad3-a123-e6db66c22b98" TYPE="apfs" PARTLABEL="TARDIS" PARTUUID="8b7a43dc-8748-4fb4-a880-ff7fb4bcfe88"
/dev/sdc3: LABEL_FATBOOT="RASPI" LABEL="SG-MOVIES" UUID="05EC-1B12" TYPE="vfat" PARTUUID="cac5542e-c865-4d37-a179-8bc92a68edf5"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/loop3: TYPE="squashfs"
/dev/loop4: TYPE="squashfs"
/dev/loop5: TYPE="squashfs"
```

---
###### 2021-03-20 

## Installing Internal 1.5T WD Drive

Okay, so I tried doing this fast and loose, and damn near lost a whole bunch of data.  Now we do it slow and steady.  

First thing I'm going to want to do is make sure we're protecting the currently function drives.  My first instinct is to simply disconnect the drives, and since they aren't necessary for boot we shouldn't need any commenting in `/etc/fstab`.  

Next thing is to compile a logical set of steps: reasearch time.  

First attempt resource is: [https://www.techrepublic.com/article/how-to-properly-automount-a-drive-in-ubuntu-linux/](). 

##### before operating

1. reset `fstab` to pre-internal drive fuckery status 
2. `sudo poweroff` 
3. moved internal from `sata1` to `sata3` and reconnected 
4. powered Moxie 

#### method

1. `sudo fdisk -l`: locate the partition to mount 

```sh
# snippet for 1.5T internal 
Disk /dev/sdc: 1.37 TiB, 1500301910016 bytes, 2930277168 sectors
Disk model: WDC WD15EARS-00Z
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe84c04a4

Device     Boot Start        End    Sectors  Size Id Type
/dev/sdc1        2048 2930274303 2930272256  1.4T  7 HPFS/NTFS/exFAT
```

2. `sudo blkid`: get partition UUID 

```sh
# snippet for 1.5T internal 
/dev/sdc1: LABEL="thebarn.2" UUID="66E8AE01E8ADD01B" TYPE="ntfs" PARTUUID="e84c04a4-01"
```

3. verify mount point in `/media/sargent/` 

```sh
# ll /media/sargent/ 
drwxrwxr--+  5 ash     media  4096 Mar 16 20:02 ./
drwxr-xr-x   4 root    root   4096 Feb 16 20:34 ../
drwxrwxr-x  11 root    media 32768 Jan  1  1970 sg-movies/
drwxrwxr--   1 sargent media  4096 Oct 14 19:29 sg-tv/
drwxrwxr--   2 sargent media  4096 Mar 16 20:02 sg-tv.1/
```

4. `sudo vi /etc/fstab`: append entry for new partition 

```sh
# line:23
UUID=66E8AE01E8ADD01B /media/sargent/sg-tv.1 auto rw,user,users,uid=1001,gid=1002,umask=003 0 0
```

5. `sudo mount -a`: check for errors in the `/etc/fstab` 

##### error returned 
```
The disk contains an unclean file system (0, 0).
Metadata kept in Windows cache, refused to mount.
Falling back to read-only mount because the NTFS partition is in an
unsafe state. Please resume and shutdown Windows fully (no hibernation
or fast restarting.)
Could not mount read-write, trying read-only
```

### first fix 

- source: [https://nixhive.com/disk-contains-unclean-file-system-0-0-ntfs-error-linux/]()

1. `ntfsfix /dev/sdc1`: repair the drive 

```
Mounting volume... Error opening read-only '/dev/sdc1': Permission denied
FAILED
Attempting to correct errors... Error opening read-only '/dev/sdc1': Permission denied
FAILED
Failed to startup volume: Permission denied
Error opening '/dev/sdc1': Read-only file system
Volume is corrupt. You should run chkdsk.
```

Alright, it looks like the disk itself is either corrupt, or so ridden with Winware that it can't communicate safely with Ubuntu.

Either way, it's time to reformat.  

- source: [https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux#:~:text=How%20To%20Partition%20and%20Format%20Storage%20Devices%20in,entire%20disk%20in%20this%20guide.%20More%20items...%20]() 

0. `cp -r /media/sargent/sg-tv.1/Anime/ ~/`: backing media up to local drive -- JK, there's just way too much stuff 

1. `sudo umount /media/sargent/sg-movies; sudo umount /media/sargent/sg-tv`: unmount and disconnect `sg-movies` and `sg-tv` to avoid huge data loss 
2. `sudo umount /media/sargent/sg-tv.1`: unmount the device so it can be partitioned 
3. `sudo parted /dev/sdc mklabel gpt`: format drive 
4. `sudo parted -a opt /dev/sdc mkpart primary ext4 0% 100%`: create new partition size and type 
5. `sudo mkfs.ext4 -L datapartition /dev/sdc1`: set partition map 
6. `sudo blkid`: get new UUID 

```sh 
# snippet 
/dev/sdc1: LABEL="datapartition" UUID="d2fce0a4-e542-44fd-bd4e-e0828091cafa" TYPE="ext4" PARTLABEL="primary" PARTUUID="e6621ad7-dd16-4627-b9fa-69ad04b8b3db"
```

9. `sudo vi /etc/fstab`: update fstab 
10. `sudo mount -a`: check for errors in the `/etc/fstab` 

```sh
# updated line:23 
UUID=d2fce0a4-e542-44fd-bd4e-e0828091cafa /media/sargent/sg-tv.1 ext4 defaults 0 2 
```

### Expectations: 

- boot time issues resolved after changing sata ports 
- 1.5T internal is now mounted to `/media/sargent/sg-tv.1` 
- 1.5T internal is writable by at least `sudo` 

### Reality:

- [✔] boot time issues resolved after changing sata ports 

`UUID=d2fce0a4-e542-44fd-bd4e-e0828091cafa /media/sargent/sg-tv.1 ext4 defaults 0 1`

Shit's working, and I'm gonna just leave it there. 

---

